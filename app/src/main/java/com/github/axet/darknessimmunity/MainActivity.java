package com.github.axet.darknessimmunity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.CircleImageView;
import com.github.axet.androidlibrary.widgets.PopupShareActionProvider;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DialogInterface.OnDismissListener {

    public static int WL_STEP = 5;

    public static int MAX = 740;
    public static int MIN = 380;

    public static float[] DEFAULTS = {
            680, // red
            605, // orange
            580, // yellow
            532.5f, // green
            472.5f, // blue
            435,// indigo
            415 // violet
    };

    public static int[] LIGHT_IDS = {
            R.id.light_1,
            R.id.light_2,
            R.id.light_3,
            R.id.light_4,
            R.id.light_5,
            R.id.light_6,
            R.id.light_7,
    };

    FloatingActionButton fab;
    CircleImageView image;
    DrawerLayout drawer;
    NavigationView navigationView;

    public static class RingButtonAnimation extends Animation {
        View button;
        LayerDrawable bg;
        Rect bounds;
        GradientDrawable inner;

        public RingButtonAnimation(View i) {
            this.button = i;
            this.bg = (LayerDrawable) this.button.getBackground();
            this.inner = (GradientDrawable) bg.getDrawable(1);
            Animation a = i.getAnimation();
            if (a != null) {
                a.reset();
                i.clearAnimation();
            }
            this.bounds = new Rect(inner.getBounds());
            setDuration(200);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            if (interpolatedTime > 0.5) {
                float i = 1 - (interpolatedTime - 0.5f) * 2;
                int xx = (int) (bounds.width() * i);
                int yy = (int) (bounds.height() * i);
                inner.setBounds(bounds.left + xx, bounds.top + yy, bounds.right - xx, bounds.bottom - yy);
            } else {
                float i = interpolatedTime * 2;
                int xx = (int) (bounds.width() * i);
                int yy = (int) (bounds.height() * i);
                inner.setBounds(bounds.left + xx, bounds.top + yy, bounds.right - xx, bounds.bottom - yy);
            }
        }

        @Override
        public void reset() {
            super.reset();
            inner.setBounds(bounds);
        }
    }

    public static class FadeTextAnimation extends Animation {
        TextView text;
        double luma;
        int textColor;

        public FadeTextAnimation(TextView text, float wl) {
            int rgb = MainApplication.waveLengthToRGB(wl);

            this.text = text;
            this.luma = MainApplication.luma(rgb);

            if (luma > 128)
                textColor = 0xff000000;
            else
                textColor = 0xffffffff;

            setDuration(2000);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            if (interpolatedTime > 0.5) {
                int a = (int) (255 * (1 - interpolatedTime));
                text.setTextColor((0x00ffffff & textColor) | (a << 24));
            } else {
                text.setTextColor(0xff000000 | textColor);
            }
        }

        @Override
        public void reset() {
            super.reset();
            text.setTextColor(0x0);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = MainApplication.getCharmUri(MainActivity.this);
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("image/jpeg");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                emailIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.github.axet.darknessimmunity");
                PopupShareActionProvider.show(MainActivity.this, fab, emailIntent);
            }
        });

        image = (CircleImageView) findViewById(R.id.image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);
                startActivity(intent);
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        updateImage();
        updateColors();
        updateIcons();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_background) {
            try {
                Uri uri = MainApplication.getCharmUri(this);

                if (MainApplication.setWallpaper(this, uri) == 0)
                    Snackbar.make(fab, getString(R.string.app_name) + " " + getString(R.string.immunity_installed), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            } catch (RuntimeException e) {
                Log.d("TAG", "SetBitmap", e);
                Snackbar.make(fab, "Error", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
            return true;
        }

        if (id == R.id.action_about) {
            AboutPreferenceCompat.showDialog(this, R.raw.about);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_reset:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(com.github.axet.darknessimmunity.R.string.reset);
                builder.setMessage(com.github.axet.darknessimmunity.R.string.areyousure);
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reset();
                    }
                });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.create().show();
                break;
            default:
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("charm", MainApplication.CHARMS_IDS.get(id));
                editor.commit();
                break;
        }

        updateImage();

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void updateIcons() {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < MainApplication.CHARMS_NAV.length; i++) {
            MenuItem item = menu.findItem(MainApplication.CHARMS_NAV[i]);
            CircleImageView v = new CircleImageView(this);

            Uri uri = MainApplication.CHARMS_URI[i];
            try {
                v.setImageURI(uri);
            } catch (OutOfMemoryError e) {
                Bitmap bm = MainApplication.scaleCenterCrop(this, uri);
                v.setImageBitmap(bm);
            }

            v.setBackgroundColor(Color.TRANSPARENT);
            v.measure(View.MeasureSpec.makeMeasureSpec(100, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(100, View.MeasureSpec.AT_MOST));
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());

            Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.draw(c);

            BitmapDrawable bg = new BitmapDrawable(getResources(), b);
            item.setIcon(bg);
        }
    }

    void updateImage() {
        int i = MainApplication.getCharm(this);
        setImageURI(MainApplication.getCharmUri(i));
        navigationView.setCheckedItem(MainApplication.CHARMS_NAV[i]);
    }

    void setImageURI(Uri uri) {
        // image.setImageURI(uri); // throws OutOfMemoryError, some phones BitmapShader+Paint gives blackscreen
        Bitmap bm = MainApplication.scaleCenterCrop(this, uri);
        image.setImageBitmap(bm);
    }

    public float getLight(int i) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences.getFloat("light_" + i, DEFAULTS[i]);
    }

    public static void setLight(Context context, int i, float wl) {
        if (wl > MAX)
            wl = MAX;
        if (wl < MIN)
            wl = MIN;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putFloat("light_" + i, wl);
        edit.commit();
    }

    void reset() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = preferences.edit();
        for (int i = 0; i < DEFAULTS.length; i++) {
            edit.putFloat("light_" + i, DEFAULTS[i]);
        }
        edit.commit();
        updateColors();
    }

    public int getAverage() {
        int r = 0, g = 0, b = 0;
        for (int i = 0; i < LIGHT_IDS.length; i++) {
            int rgb = MainApplication.waveLengthToRGB(getLight(i));
            r += Math.pow(255 - (rgb & 0x00ff0000) >> 16, 2);
            g += Math.pow(255 - (rgb & 0x0000ff00) >> 8, 2);
            b += Math.pow(255 - (rgb & 0x000000ff), 2);
        }

        r = r / LIGHT_IDS.length;
        g = g / LIGHT_IDS.length;
        b = b / LIGHT_IDS.length;

        r = Math.min(255, (int) Math.sqrt(r));
        g = Math.min(255, (int) Math.sqrt(g));
        b = Math.min(255, (int) Math.sqrt(b));

        return 0xff000000 | (r << 16) | (g << 8) | (b);
    }

    void updateColors() {
        for (int i = 0; i < LIGHT_IDS.length; i++) {
            updateColor(i, findViewById(LIGHT_IDS[i]), getLight(i), false);
        }
        updateCenter();
    }

    void updateCenter() {
        Point size = MainApplication.getSize(this);

        int startAlpha = 0x40; // from white color
        int endAlpha = 0xc0; // to average color

        int rgb = (getAverage() & 0x00ffffff) | (endAlpha << 24);
        View content = findViewById(R.id.gradient);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{rgb, 0x00ffffff | (startAlpha << 24)});
        gd.setGradientType(GradientDrawable.RADIAL_GRADIENT);
        gd.setGradientRadius(Math.min(size.x, size.y));
        content.setBackgroundDrawable(gd);

        image.setBorderColor(0xff000000 | rgb);
    }

    void updateColor(final int i, View v, final float wl, boolean animate) {
        int rgb = MainApplication.waveLengthToRGB(wl);

        final TextView light = (TextView) v.findViewById(R.id.light);
        OvalShape shape = new OvalShape();
        ShapeDrawable bg = new ShapeDrawable();
        bg.setShape(shape);
        bg.getPaint().setColor(rgb);
        light.setBackgroundDrawable(bg);
        light.setText(wl + " nm");
        light.setTextColor(0x0);
        light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LightDialog dialog = new LightDialog();
                Bundle args = new Bundle();
                args.putFloat("wl", wl);
                args.putInt("light", i);
                dialog.setArguments(args);
                dialog.show(getSupportFragmentManager(), "");
            }
        });

        final View minus = v.findViewById(R.id.button_minus);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float wl = getLight(i);
                wl -= WL_STEP;
                setLight(MainActivity.this, i, wl);
                updateColors();
                light.startAnimation(new FadeTextAnimation(light, wl));
                minus.startAnimation(new RingButtonAnimation(minus));
            }
        });

        final View plus = v.findViewById(R.id.button_plus);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float wl = getLight(i);
                wl += WL_STEP;
                setLight(MainActivity.this, i, wl);
                updateColors();
                light.startAnimation(new FadeTextAnimation(light, wl));
                plus.startAnimation(new RingButtonAnimation(plus));
            }
        });

        if (animate)
            light.startAnimation(new FadeTextAnimation(light, wl));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dialog instanceof LightDialog.Result) {
            LightDialog.Result r = (LightDialog.Result) dialog;
            float wl = r.wl;
            int i = r.light;
            View v = findViewById(LIGHT_IDS[i]);
            updateColor(i, v, wl, true);
            updateCenter();
        }
    }
}
