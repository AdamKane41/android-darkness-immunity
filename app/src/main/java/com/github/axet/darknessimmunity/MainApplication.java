package com.github.axet.darknessimmunity;

import android.app.Application;
import android.app.WallpaperManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MainApplication extends Application {
    public static String TAG = MainApplication.class.getSimpleName();

    public static Uri CHARM = Uri.parse("content://com.github.axet.darknessimmunity/charm.jpg");

    public static int CHARM_DEFAULT = 0;

    static int[] CHARMS_NAV = {
            R.id.nav_charm
    };

    static Uri[] CHARMS_URI = {
            CHARM
    };

    static Map<Integer, Integer> CHARMS_IDS = new TreeMap<>();

    static {
        for (int i = 0; i < CHARMS_NAV.length; i++) {
            CHARMS_IDS.put(CHARMS_NAV[i], i);
        }
    }

    // @user151323
    public static int waveLengthToRGB(double wl) {
        double gamma = 0.80;
        double intensity = 255;
        double factor;
        double red, green, blue;

        if ((wl >= 380) && (wl < 440)) {
            red = -(wl - 440) / (440 - 380);
            green = 0.0;
            blue = 1.0;
        } else if ((wl >= 440) && (wl < 490)) {
            red = 0.0;
            green = (wl - 440) / (490 - 440);
            blue = 1.0;
        } else if ((wl >= 490) && (wl < 510)) {
            red = 0.0;
            green = 1.0;
            blue = -(wl - 510) / (510 - 490);
        } else if ((wl >= 510) && (wl < 580)) {
            red = (wl - 510) / (580 - 510);
            green = 1.0;
            blue = 0.0;
        } else if ((wl >= 580) && (wl < 645)) {
            red = 1.0;
            green = -(wl - 645) / (645 - 580);
            blue = 0.0;
        } else if ((wl >= 645) && (wl < 781)) {
            red = 1.0;
            green = 0.0;
            blue = 0.0;
        } else {
            red = 0.0;
            green = 0.0;
            blue = 0.0;
        }

        // Let the intensity fall off near the vision limits
        if ((wl >= 380) && (wl < 420)) {
            factor = 0.3 + 0.7 * (wl - 380) / (420 - 380);
        } else if ((wl >= 420) && (wl < 701)) {
            factor = 1.0;
        } else if ((wl >= 701) && (wl < 781)) {
            factor = 0.3 + 0.7 * (780 - wl) / (780 - 700);
        } else {
            factor = 0.0;
        }

        // Don't want 0^x = 1 for x <> 0
        int r = red == 0.0 ? 0 : (int) Math.round(intensity * Math.pow(red * factor, gamma));
        int g = green == 0.0 ? 0 : (int) Math.round(intensity * Math.pow(green * factor, gamma));
        int b = blue == 0.0 ? 0 : (int) Math.round(intensity * Math.pow(blue * factor, gamma));

        return 0xff000000 | (r << 16) | (g << 8) | (b);
    }

    public static int getCharm(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        int i = preferences.getInt("charm", CHARM_DEFAULT);
        if (i >= CHARMS_NAV.length)
            i = CHARM_DEFAULT;
        return i;
    }

    public static Uri getCharmUri(int i) {
        if (i >= CHARMS_URI.length)
            i = CHARM_DEFAULT;
        return CHARMS_URI[i];
    }

    public static Uri getCharmUri(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Uri uri = getCharmUri(preferences.getInt("charm", CHARM_DEFAULT));
        return uri;
    }

    static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }

    public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    public static Bitmap resizeBitmap(Context context, Uri resId, int reqWidth, int reqHeight) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(context.getContentResolver().openInputStream(resId), null, options);

            if (options.outHeight == -1 || options.outWidth == 1 || options.outMimeType == null) {
                return null;
            }

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;

            Bitmap bm = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(resId), null, options);
            Bitmap old = bm;
            bm = scaleCenterCrop(bm, reqWidth, reqHeight);
            old.recycle();
            return bm;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    public static Point getSize(Context context) {
        int width = 0;
        int height = 0;

        WindowManager w = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (Build.VERSION.SDK_INT >= 13) {
            Point size = new Point();
            w.getDefaultDisplay().getSize(size);
            switch (w.getDefaultDisplay().getRotation()) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    width = size.x;
                    height = size.y;
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    width = size.y;
                    height = size.x;
                    break;
            }
        } else {
            Display d = w.getDefaultDisplay();
            width = d.getWidth();
            height = d.getHeight();
        }

        return new Point(width, height);
    }

    public static void setOldWallpaper(Context context, Uri uri) {
        try {
            Point size = getSize(context);

            WallpaperManager wm = WallpaperManager.getInstance(context);
            int dw = wm.getDesiredMinimumWidth();
            int dh = wm.getDesiredMinimumHeight();
            Bitmap bm;
            if (size.y < dh || size.x < dw) {  // old phones multi screen wallpaper
                try {
                    bm = resizeBitmap(context, uri, dw, dh);
                } catch (OutOfMemoryError e) {
                    bm = resizeBitmap(context, uri, size.x, size.y);
                }
            } else {
                bm = resizeBitmap(context, uri, size.x, size.y);
            }
            wm.setBitmap(bm);
            bm.recycle();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static int setWallpaper(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT >= 19) {
            try {
                WallpaperManager wm = WallpaperManager.getInstance(context);
                Intent i = wm.getCropAndSetWallpaperIntent(uri);
                context.startActivity(i);
                return 1;
            } catch (IllegalArgumentException e) { // Cannot use passed URI to set wallpaper; check that the type returned by ContentProvider matches image/*
                Log.d(TAG, "setWallpaper", e);
                MainApplication.setOldWallpaper(context, uri);
                return 0;
            } catch (OutOfMemoryError e) { // 19-21 crashing out of memory
                Log.d(TAG, "setWallpaper", e);
                MainApplication.setOldWallpaper(context, uri);
                return 0;
            }
        } else {
            MainApplication.setOldWallpaper(context, uri);
            return 0;
        }
    }

    public static Bitmap scaleCenterCrop(Context context, Uri uri) {
        Point size = getSize(context);
        return MainApplication.resizeBitmap(context, uri, size.x, size.y);
    }

    public static double luma(int rgb) {
        return 0.2126 * ((rgb & 0x00ff0000) >> 16) + 0.7152 * ((rgb & 0x0000ff00) >> 8) + 0.0722 * (rgb & 0x000000ff);
    }
}
